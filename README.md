# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo contains code for iOS version of Flix. Sample app which includes movie listing using the moviedb
Version 1.0

### How do I get set up? ###

In order to get the app running , Pull code from nishad/release
Install cocoa Pods, Install/embed realmSwift framework (refer realm guide)
These steps would be enough to get the app up and running

Features added :
* Movie listing with pagination
* Movie detail view
* Youtube player for playing movie trailers
* Search through loaded movies

### Terminologies used within the app and more about structuring
App contains 3 base folders - Appfiles, AppModules and Helpers
Xcode generated files are to be found in Appfiles
Helpers contain Utilities and custom classes used for convinience
AppModules contain the core logic of the app, where each module (Movie List, Movie details, Search) is sub divided into Model, View and View models

Each module has its own storyboard , main storyboard is used to setup tabbar controller.
App uses Navigation controller throughout

Term VC refers to View Controller
### Who do I talk to? ###

In case if anything is malfunctioning please feel free to revert back to me on nishaddangui3@gmail.com